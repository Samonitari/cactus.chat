---
title: "Client"
description: "Reference for the Cactus Comments client."
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
draft: false
images: []
menu:
  docs:
    parent: "client"
weight: 21
toc: true
---

The client consists of a script and a style sheet. The script is an IIFE that registers the `initComments` function in the global scope which can be called from within any other script. Calling it initialised the comment section given by the provided argument.

If guest posting isn't disabled, then the client registers a guest account with the homeserver on initialisation and stores the access token in HTML5 localStorage which is re-used across pages and sessions. Note, the homeserver needs to allow guest registrations for this to work.


## Configuration

The `initComments` function takes an object as argument with the following options:

| Name                   | Type                   | Required | Default | Description                                                                                                                                                                          |
|------------------------|------------------------|-----------|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `node`                 | HTML Element or string | Yes       | -       | The HTML element on the page that will be replaced with the comment section. A string is passed as the selector to `document.querySelector` (from v0.9.0).                           |
| `defaultHomeserverUrl` | string                 | Yes       | -       | The URL of the Matrix homeserver with Cactus Comments appservice. It must not contain a trailing slash.                                                                              |
| `serverName`           | string                 | Yes       | -       | The server name of the Matrix homeserver with Cactus Comments appservice. This is usually just the domain part of `defaultHomeserverUrl`.                                            |
| `siteName`             | string                 | Yes       | -       | The unique identifier of your site that you registered with the Cactusbot.                                                                                                           |
| `commentSectionId`     | string                 | Yes       | -       | The unique identifier of your comment section. It must not contain slashes and underscores.                                                                                          |
| `pageSize`             | int                    | No        | 10      | The number of comments to load initially and with every press of "View More".                                                                                                        |
| `loginEnabled`         | bool                   | No        | true    | If true, users can log in on the page using their Matrix credentials. If false, the Login button becomes a Matrix.to link to redirect them to a Matrix client. Introduced in v0.7.0. |
| `guestPostingEnabled`  | bool                   | No        | true    | If true, users can post as a guest without logging in. Introduced in v0.7.0.                                                                                                         |
| `updateInterval`       | float                  | No        | 0       | Number of seconds after which new comments are fetched. If zero or negative, updates are never fetched. Introduced in v0.8.0.                                                        |

When using data attributes, the `node` parameter becomes optional. It defaults to replacing the `script` element itself with a comment section. Also the data attributes are prefixed with `data-` and use kebab-casing, where `initComments` uses camelCasing.


## Architecture

The Cactus Comments web client is written in [Elm](https://elm-lang.org/). It is is a proper Matrix client, and as such implements the [Client-Server API](https://matrix.org/docs/spec/client_server/r0.6.1). You can read and [contribute](../../community/contribute/) to the entire source code [here](https://gitlab.com/cactus-comments/cactus-appservice).